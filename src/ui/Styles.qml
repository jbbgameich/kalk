import QtQuick 2.0
// import QtQuick.Controls.Material 2.0

Item {
    // Colors
//     property string accentColor: Material.color(Material.Pink, Material.Shade500)

    // Font sizes
    property int fontSize: root.height / 9
    property int advancedFontSize: fontSize
    property int historyFontSize: fontSize

    // Font opacities
    property double primaryTextOpacity: 1
    property double secondaryTextOpacity: 1
    property double hintTextOpacity: 1
}
